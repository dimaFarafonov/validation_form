const error = {
  day: {
    is_required: "DAY IS REQUIRED",
    is_number: "DAY MUST BE A NUMBER",
    is_ranged: "DAY MUST BE FROM 1 TO 31",
  },
  mounth: {
    is_required: "MOUTH REQUIRED",
    is_number: "MOUTH MUST BE A NUMBER",
    is_ranged: "MOUTH MUST BE FROM 1 TO 12",
  },
  year: {
    is_required: "YEAR IS REQUIRED",
    is_number: "YEAR MUST BE A NUMBER",
    is_ranged: "PLEASE ENTER VALID NUMBER OF YEAR FROM 1900 TO 2020",
    is_old: "SORRY, YOU MUST BE AT LEAST 18 YEARS OLD",
  },
};
export default error;
