const error = {
  email: {
    is_required: "EMAIL IS REQUIRED",
    is_invalid: "INVALID EMAIL ADDRESS",
  },
  password: {
    is_required: "PASSWORD IS REQUIRED",
    is_valid: "PASSWORD SHOULD BE AT LEAST 6 CHARACTERS LONG",
  },
  confirm_password: { is_valid: "PASSWORDS NOT MATCHING" },
};
export default error;
