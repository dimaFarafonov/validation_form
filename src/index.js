import React from "react";
import ReactDOM from "react-dom";
import App from "../src/components/App";

import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import progress_bar from "./components/progress_bar/reducer";
import user_data from "./reducer";

const rootReducer = combineReducers({
  progress_bar: progress_bar,
  user_data: user_data,
  form: formReducer,
});
const store = createStore(rootReducer);
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector("#root")
);
