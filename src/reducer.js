const user_state = {
  email: null,
  password: null,
  confirmed_password: null,
  day: null,
  mounth: null,
  year: null,
  gender: null,
  about: null,
};

export default function (state = user_state, action) {
  switch (action.type) {
    case "START_FORM_DATA":
      return {
        ...state,
        email: action.email,
        password: action.password,
        confirmed_password: action.confirmed_password,
      };
    case "PERSONAL_FORM_DATA":
      return Object.assign({}, state, {
        day: action.day,
        mounth: action.mounth,
        year: action.year,
        gender: action.gender,
        about: action.about,
      });
    default:
      return state;
  }
}
