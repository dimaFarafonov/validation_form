import React, { Component } from "react";

import styles from "../../css/start_form.module.css";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { setStartFormData } from "../../actions";
import { updateProgress } from "./../progress_bar/actions";
import error from "../../config_errors/start_form";
const validate = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = error.email.is_required;
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = error.email.is_invalid;
  }
  if (!values.password) {
    errors.password = error.password.is_required;
  } else if (values.password.length < 6) {
    errors.password = error.password.is_valid;
  }
  if (values.password !== values.confirm_password || !values.confirm_password) {
    errors.confirm_password = error.confirm_password.is_valid;
  }
  return errors;
};
const renderField = ({
  input,
  label,
  type,
  name,
  meta: { touched, error, warning },
}) => (
  <div>
    <div className={styles.field}>
      <label className={styles.padding_left}>
        {input.name === "email"
          ? (def = "EMAIL")
          : input.name === "password"
          ? (def = "PASSWORD")
          : input.name === "confirm_password"
          ? (def = "CONFIRM PASSWORD")
          : null}
      </label>
      <input
        {...input}
        placeholder={label}
        type={type}
        className={styles.input_focus}
      />
      <div className={styles.padding_left}>
        <div className={styles.red}>
          {touched &&
            ((error && <span>{error}</span>) ||
              (warning && <span>{warning}</span>))}
        </div>
      </div>
    </div>
  </div>
);
let def = "";
class RegisterForm extends Component {
  componentDidMount() {
    const { email, password, confirmed_password } = this.props.user_data;
    this.props.initialize({
      email: email,
      password: password,
      confirm_password: confirmed_password,
    });
  }
  render() {
    const { handleSubmit } = this.props;

    const submit = (values) => {
      this.props.setStartFormData(
        values.email,
        values.password,
        values.confirm_password
      );
      this.props.updateProgress(66.6);
    };

    return (
      <div>
        <form onSubmit={handleSubmit(submit)} className={styles.form}>
          <Field name="email" component={renderField} type="text" />
          <Field name="password" component={renderField} type="password" />
          <Field
            name="confirm_password"
            component={renderField}
            type="password"
          />
          <div className={styles.button_block}>
            <hr />
            <button type="submit" className={styles.button_focus}>
              Next
            </button>
          </div>
        </form>
      </div>
    );
  }
}
RegisterForm = reduxForm({
  validate,
  form: "register_form",
})(RegisterForm);
const mapStateToProps = (state) => {
  const { user_data } = state;
  return {
    user_data,
  };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateProgress,
      setStartFormData,
    },
    dispatch
  );
export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm);
