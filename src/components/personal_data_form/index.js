import React, { Component } from "react";
import styles from "../../css/personal_form.module.css";
import { Field, reduxForm, getFormMeta, getFormSyncErrors } from "redux-form";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import error from "../../config_errors/second_form";
import { Select, MenuItem, FormControl } from "@material-ui/core";
import { setPersonalFormData } from "../../actions";
import { updateProgress } from "./../progress_bar/actions";

const gender_select = {
  backgroundColor: "rgb(62, 122, 235)",
  color: "white",
};

const validate = (values, { formMeta }) => {
  const activeFieldName = formMeta.date && Object.keys(formMeta.date).find(name => formMeta.date[name].active)
  const errors = {};
  if (!values.date) {
    errors.date = " fddfdf"
    return errors
  }
  if (activeFieldName === 'day') {
    if (!values.date.day) {
      errors.date = error.day.is_required;
    } else if (isNaN(Number(values.date.day))) {
      errors.date = error.day.is_number;
    } else if (!(values.date.day >= 1 && values.date.day <= 31)) {
      errors.date = error.day.is_ranged;
    }
  }

  if (activeFieldName === 'mounth') {
    if (!values.date.mounth) {
      errors.date = error.mounth.is_required;
    } else if (isNaN(Number(values.date.mounth))) {
      errors.date = error.mounth.is_number;
    } else if (!(values.date.mounth >= 1 && values.date.mounth <= 12)) {
      errors.date = error.mounth.is_ranged;
    }
  }
  if (activeFieldName === 'year') {
    if (!values.date.year) {
      errors.date = error.year.is_required;
    } else if (isNaN(Number(values.date.year))) {
      errors.date = error.year.is_number;
    } else if (
      !(values.date.year >= 1900 && values.date.year <= new Date().getFullYear())
    ) {
      errors.date = error.year.is_ranged;
    } else if (new Date().getFullYear() - values.date.year < 18) {
      errors.date = error.year.is_old;
    }
  }
  return errors;
};
const about = [
  {
    value: "Surfing internet, searching a job",
  },
  {
    value: "Claiming daily newspaper from backyard",
  },
  {
    value: "Watching TV-show",
  },
];
const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning },
}) => (
  <div>
    <div>
      <input
        {...input}
        placeholder={label}
        type={type}
        className={styles.personal}
      />
    </div>
  </div>
);

class PersonalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: "DATE OF BIRTH",
      selected_male: gender_select,
    };
  }
  componentDidMount() {
    const { day, mounth, year } = this.props.user_data;
    this.props.initialize({
      day: day,
      mounth: mounth,
      year: year,
    });
  }
  render() {
    const { formMeta, formError } = this.props
    console.log(formError);
    const submit = (values) => {
      let gender = "MALE";
      if (this.state.selected_female) {
        gender = "FEMALE";
      } else if (this.state.selected_unsp) {
        gender = "UNSPECIFIED";
      }
      this.props.setPersonalFormData(
        values.day,
        values.mounth,
        values.year,
        gender,
        this.state.about_us || null
      );
      this.props.updateProgress(100);
    };
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={handleSubmit(submit)} className={styles.form}>
        <label className={styles.padding}>{this.state.date}</label>
        <div className={styles.field_block}>
          <Field
            label={"DD"}
            className={styles.personal}
            name="date.day"
            type="text"
            component={renderField}
          />
          <Field
            component={renderField}
            label={"MM"}
            className={styles.personal}
            name="date.mounth"
            type="text"
          />
          <Field
            name="date.year"
            type="text"
            component={renderField}
            className={styles.personal}
            label={"YYYY"}
          />
        </div>
        {formMeta.date && Object.values(formMeta.date).some(({ touched }) => touched) && formError.date && <div style={{ color: "red", top: 10 }}>{formError.date}</div>}

        <label className={styles.padding}>GENDER</label>
        <div className={styles.field_block}>
          <button
            type="button"
            className={styles.personal}
            onClick={() => {
              delete this.state.selected_female;
              delete this.state.selected_unsp;
              this.setState({
                selected_male: gender_select,
              });
            }}
            style={Object.assign({}, {}, this.state.selected_male)}
          >
            MALE
          </button>
          <button
            type="button"
            className={styles.personal}
            onClick={() => {
              delete this.state.selected_male;
              delete this.state.selected_unsp;
              this.setState({
                selected_female: gender_select,
              });
            }}
            style={Object.assign({}, {}, this.state.selected_female)}
          >
            FEMALE
          </button>
          <button
            type="button"
            className={styles.personal}
            onClick={() => {
              delete this.state.selected_male;
              delete this.state.selected_female;
              this.setState({
                selected_unsp: gender_select,
              });
            }}
            style={Object.assign({}, {}, this.state.selected_unsp)}
          >
            UNSPECIFIED
          </button>
        </div>
        <FormControl>
          <div className={styles.about_us}>WHERE DID YOU HEAR ABOUT US?</div>
          <Select
            className={styles.select}
            value={this.state.age}
            onChange={(e) => {
              this.setState({
                age: e.target.value,
                about_us: about[e.target.value].value,
              });
            }}
          >
            <MenuItem value=""></MenuItem>
            {about.map((option, index) => {
              return <MenuItem value={index}>{option.value}</MenuItem>;
            })}
          </Select>
        </FormControl>
        <div className={styles.form_footer}>
          <hr />
          <div className={styles.buttons_block}>
            <button
              onClick={() => this.props.updateProgress(33.3)}
              type="button"
              className={styles.button_focus_back}
            >
              Back
            </button>
            <button className={styles.button_focus}>Next</button>
          </div>
        </div>
      </form>
    );
  }
}
PersonalForm = reduxForm({
  form: "second_step",
  validate,
})(PersonalForm);
const mapStateToProps = (state) => {
  const { progress_bar, user_data } = state;
  return {
    progress_bar,
    user_data,
    formMeta: getFormMeta('second_step')(state),
    formError: getFormSyncErrors('second_step')(state),
  };
};
const mapDispatchToProps = {
    updateProgress,
    setPersonalFormData,
  };
export default connect(mapStateToProps, mapDispatchToProps)(PersonalForm);
