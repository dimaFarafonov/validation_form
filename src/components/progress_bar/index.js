import React, { Component } from "react";
import styles from "../../css/progress_bar.module.css";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateProgress } from "./actions";
class ProgressBar extends Component {
  render() {
    return (
      <div className={styles.block}>
        <div className={styles.progress_div}>
          <div
            style={{ width: `${this.props.progress_bar.percentage}%` }}
            className={styles.progress}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { progress_bar } = state;
  return { progress_bar };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateProgress,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ProgressBar);
