export const updateProgress = (percentage) => ({
  type: "UPDATE_PROGRESS",
  percentage: percentage,
});
export const getError = (error) => ({
  type: "GET_ERROR",
  error: error,
});
