const bar_state = {
  percentage: 33.3,
  error: "",
};

export default function (state = bar_state, action) {
  switch (action.type) {
    case "UPDATE_PROGRESS":
      return Object.assign({}, state, {
        percentage: action.percentage,
      });
    case "GET_ERROR":
      return Object.assign({}, state, {
        error: action.error,
      });
    default:
      return state;
  }
}
