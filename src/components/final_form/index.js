import React, { Component } from "react";
import styles from "../../css/start_form.module.css";

import check_mark from "./../../assets/check.png";
import { setStartFormData } from "../../actions";
import { updateProgress } from "./../progress_bar/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class DashBoard extends Component {
  componentDidMount() {}
  render() {
    return (
      <div className={styles.final_block}>
        <img alt="check" src={check_mark} width="200px" height="200px" />
        <button
          type="button"
          className={styles.button_focus}
          onClick={() => {
            console.log("this.props.user_data", this.props.user_data);
          }}
        >
          Go to dashboard
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { progress_bar, user_data } = state;
  return {
    progress_bar,
    user_data,
  };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateProgress,
      setStartFormData,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(DashBoard);
