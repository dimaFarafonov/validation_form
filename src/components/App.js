import React, { Component } from "react";
import RegisterForm from "./start_form";
import ProgressBar from "./../components/progress_bar";
import PersonalForm from "./personal_data_form";
import FinalForm from "./final_form";
import styles from "../css/start_form.module.css";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "fontsource-roboto";

class App extends Component {
  render() {
    return (
      <div className={styles.app}>
        <h2 className={styles.h2}>Signup</h2>
        <ProgressBar />
        {this.props.progress_bar.percentage === 33.3 ? (
          <RegisterForm onSubmit={this.submit} />
        ) : null}
        {this.props.progress_bar.percentage === 66.6 ? (
          <PersonalForm onSubmit={this.submit} />
        ) : null}
        {this.props.progress_bar.percentage === 100 ? <FinalForm /> : null}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { progress_bar } = state;
  return { progress_bar };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // updateProgress,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
