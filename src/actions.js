export const setStartFormData = (email, password, confirmed_password) => ({
  type: "START_FORM_DATA",
  email: email,
  password: password,
  confirmed_password: confirmed_password,
});
export const setPersonalFormData = (day, mounth, year, gender, about) => ({
  type: "PERSONAL_FORM_DATA",
  day: day,
  mounth: mounth,
  year: year,
  gender: gender,
  about: about,
});
